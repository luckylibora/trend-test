/**
 * Created by lucky on 29.10.16.
 */


/**
 *
 * @type {{incrementing: number, notIncrementing: number}}
 */
var states = {
    incrementing: 1,
    notIncrementing: 2
};


/**
 *
 * @param {number} n
 * @returns {boolean}
 */
function checkPositiveInt(n) {
    return Number(n) == n && Math.floor(n) == n && n > 0;
}


/**
 *
 * @param {Array.<number>} a
 * @returns {Array.<number>}
 */
function removeDuplicates(a) {
    if (!a.length) {
        return [];
    }
    var res = [a[0]];
    if (a.length == 1) {
        return res;
    }
    for (var i = 1; i < a.length; i++) {
        if (a[i] != a[i - 1]) {
            res.push(a[i]);
        }
    }
    return res;
}


/**
 *
 * @param {Array.<number>} a
 * @param {function(Error = null, string = null)} cb
 */
function minify(a, cb) {
    if (!cb || typeof cb != 'function') {
        throw new Error('Callback is not function');
    }
    if (!Array.isArray(a)) {
        return cb(new Error('Argument should be Array'));
    }
    if (!a.length) {
        return cb(null, '');
    }
    var temp = removeDuplicates(a);
    if (!checkPositiveInt(temp[0])) {
        return cb(new Error('Not valid element at position 0'));
    }
    var res = temp[0].toString();
    var lastAdded = temp[0];
    var prev;
    var cur;
    var state = null;
    for (var i = 1; i < temp.length; i++) {
        prev = temp[i - 1];
        cur = temp[i];
        if (prev > cur) {
            return cb(new Error('Array is not sorted'));
        }
        if (!checkPositiveInt(cur)) {
            return cb(new Error('Not valid element at position ' + i));
        }
        if (cur - prev <= 1) {
            if (state == states.notIncrementing) {
                res += ',' + prev;
                lastAdded = prev;
            }
            state = states.incrementing;
        } else {
            if (state == states.incrementing) {
                if (prev - lastAdded > 1) {
                    res += '-' + prev;

                } else {
                    res += ',' + prev;
                }
                lastAdded = prev;
            } else if (state == states.notIncrementing) {
                res += ',' + prev;
                lastAdded = prev;
            }
            state = states.notIncrementing;
        }
    }
    if (temp.length == 1) {
        return cb(null, res);
    }
    if (temp.length == 2) {
        return cb(null, res + ',' + cur);
    }
    if (state == states.incrementing) {
        if (cur - lastAdded > 1) {
            res += '-';
        } else {
            res += ',';
        }
    } else if (state == states.notIncrementing) {
        res += ',';
    }
    res += cur;
    return cb(null, res);
}


exports.minify = minify;


