/**
 * Created by lucky on 29.10.16.
 */

var chai = require('chai');
var index = require('../index');

var assert = chai.assert;
var expect = chai.expect;


describe('Index', function () {
    describe('#minify()', function () {
        it('Basic test should return "1-8"', function (done) {
            index.minify([1, 2, 3, 4, 5, 6, 7, 8], function (err, res) {
                assert.equal(res, '1-8');
                done();
            });
        });
        it('Basic test should return "1,3-8"', function (done) {
            index.minify([1, 3, 4, 5, 6, 7, 8], function (err, res) {
                assert.equal(res, '1,3-8');
                done();
            });
        });
        it('Basic test should return "1,3-8,10-12"', function (done) {
            index.minify([1, 3, 4, 5, 6, 7, 8, 10, 11, 12], function (err, res) {
                assert.equal(res, '1,3-8,10-12');
                done();
            });
        });
        it('Basic test should return "1-3"', function (done) {
            index.minify([1, 2, 3], function (err, res) {
                assert.equal(res, '1-3');
                done();
            });
        });
        it('Basic test should return "1,2"', function (done) {
            index.minify([1, 2], function (err, res) {
                assert.equal(res, '1,2');
                done();
            });
        });
        it('Basic test should return "1,2,4"', function (done) {
            index.minify([1, 2, 4], function (err, res) {
                assert.equal(res, '1,2,4');
                done();
            });
        });
        it('Basic test should return "1,2,4-6"', function (done) {
            index.minify([1, 2, 4, 5, 6], function (err, res) {
                assert.equal(res, '1,2,4-6');
                done();
            });
        });
        it('Basic test should return "1-3,7-9,15,17,19-21"', function (done) {
            index.minify([1, 2, 3, 7, 8, 9, 15, 17, 19, 20, 21], function (err, res) {
                assert.equal(res, '1-3,7-9,15,17,19-21');
                done();
            });
        });
        it('Basic test should return "1-6,100,1091,1999-2002"', function (done) {
            index.minify([1, 2, 3, 4, 5, 6, 100, 1091, 1999, 2000, 2001, 2002], function (err, res) {
                assert.equal("1-6,100,1091,1999-2002", res);
                done();
            });
        });
        it('Basic test should return "1"', function (done) {
            index.minify([1], function (err, res) {
                assert.equal(res, '1');
                done();
            });
        });
        it('Basic test should return "1,3,5,7,9,11"', function (done) {
            index.minify([1, 3, 5, 7, 9, 11], function (err, res) {
                assert.equal(res, '1,3,5,7,9,11');
                done();
            });
        });
        it('Basic test should return "1,3,5,7,9,11,12"', function (done) {
            index.minify([1, 3, 5, 7, 9, 11, 12], function (err, res) {
                assert.equal(res, '1,3,5,7,9,11,12');
                done();
            });
        });
        it('Duplicates test should return "1"', function (done) {
            index.minify([1, 1], function (err, res) {
                assert.equal(res, '1');
                done();
            });
        });
        it('Duplicates test should return "1"', function (done) {
            index.minify([1, 1, 1, 1], function (err, res) {
                assert.equal(res, '1');
                done();
            });
        });
        it('Duplicates test should return "1,3,4"', function (done) {
            index.minify([1, 1, 3, 4], function (err, res) {
                assert.equal(res, '1,3,4');
                done();
            });
        });
        it('Duplicates test should return "1,3,4"', function (done) {
            index.minify([1, 3, 4, 4], function (err, res) {
                assert.equal(res, '1,3,4');
                done();
            });
        });
        it('Duplicates test should return "1,3,4"', function (done) {
            index.minify([1, 1, 3, 4, 4], function (err, res) {
                assert.equal(res, '1,3,4');
                done();
            });
        });
        it('Duplicates test should return "1,3,4"', function (done) {
            index.minify([1, 1, 3, 3, 4, 4], function (err, res) {
                assert.equal(res, '1,3,4');
                done();
            });
        });
        it('Empty array test', function (done) {
            index.minify([], function (err, res) {
                assert.equal(res, '');
                done();
            });
        });
        it('Number argument test', function (done) {
            index.minify(1, function (err, res) {
                assert.equal(err.message, 'Argument should be Array');
                assert.equal(res, null);
                done();
            });
        });
        it('String argument test', function (done) {
            index.minify('14242', function (err, res) {
                assert.equal(err.message, 'Argument should be Array');
                assert.equal(res, null);
                done();
            });
        });
        it('Null argument test', function (done) {
            index.minify(null, function (err, res) {
                assert.equal(err.message, 'Argument should be Array');
                assert.equal(res, null);
                done();
            });
        });
        it('Not positive array entries test', function (done) {
            index.minify([-1, 2, 3], function (err, res) {
                assert.equal(err.message, 'Not valid element at position 0');
                assert.equal(res, null);
                done();
            });
        });
        it('Zero array entries test', function (done) {
            index.minify([0, 2, 3], function (err, res) {
                assert.equal(err.message, 'Not valid element at position 0');
                assert.equal(res, null);
                done();
            });
        });
        it('Not integer array first entry test', function (done) {
            index.minify([1.4, 2, 3], function (err, res) {
                assert.equal(err.message, 'Not valid element at position 0');
                assert.equal(res, null);
                done();
            });
        });
        it('Not integer array last entry test', function (done) {
            index.minify([1, 2, 3.5], function (err, res) {
                assert.equal(err.message, 'Not valid element at position 2');
                assert.equal(res, null);
                done();
            });
        });
        it('Not integer array entries test', function (done) {
            index.minify([1, 2.2, 3], function (err, res) {
                assert.equal(err.message, 'Not valid element at position 1');
                assert.equal(res, null);
                done();
            });
        });
        it('Not sorted array test', function (done) {
            index.minify([1, 6, 3], function (err, res) {
                assert.equal(err.message, 'Array is not sorted');
                assert.equal(res, null);
                done();
            });
        });
        it('Not sorted array at first entry test', function (done) {
            index.minify([7, 6, 9], function (err, res) {
                assert.equal(err.message, 'Array is not sorted');
                assert.equal(res, null);
                done();
            });
        });
        it('Not sorted array at last entry test', function (done) {
            index.minify([5, 6, 4], function (err, res) {
                assert.equal(err.message, 'Array is not sorted');
                assert.equal(res, null);
                done();
            });
        });
        it('Not function callback', function () {
            expect(function(){
                index.minify([5, 6, 4], null);
            }).to.throw('Callback is not function')
        });
        it('Not function callback', function () {
            expect(function(){
                index.minify([5, 6, 4]);
            }).to.throw('Callback is not function')
        });
        it('Not function callback', function () {
            expect(function(){
                index.minify([5, 6, 4], {});
            }).to.throw('Callback is not function')
        });
    });
});
