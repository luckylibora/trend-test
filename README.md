Array minifier

Converts [1,2,3]  to '1-3'

**Installation**


```
#!console

npm install https://luckylibora@bitbucket.org/luckylibora/trend-test.git
```



**Usage**


```
#!javascript

var trendTest = require('trend-test');

trendTest.minify([1,2,3], function(err, res) {
        console.log(res);  //Outputs '1-3'
});
```

**Test**


```
#!console

npm test
```
